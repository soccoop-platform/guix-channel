# Guix Channel

[*Guix*](https://github.com/tribals/katas/tree/main/install-guix) - это дистрибутив ОС и одновременно средство управления пакетами программного обеспечения.

Этот репозиторий содержит определения пакетов программного обеспечения, которые производит коллектив разработчиков Платформы Соцкооп. Определения требуются для установки этого ПО на сервер, также работающий под управлением этого дистрибутива - Guix System Distribution (SD).

## [Channel Authentication](https://guix.gnu.org/manual/en/html_node/Channel-Authentication.html)

Чтобы получить ПО, распространяемое в этом канале, нужно [добавить его в список используемых каналов](https://guix.gnu.org/manual/en/html_node/Specifying-Additional-Channels.html):

```scheme
(...
  (channel
    (name 'soccoop)
    (url "https://gitlab.com/soccoop-platform/guix-channel.git")
    (branch "main")
    (commit
      "733a9726b097171f971a1687cd51ab7fc465ef7a")
    (introduction
      (make-channel-introduction
        "733a9726b097171f971a1687cd51ab7fc465ef7a"
        (openpgp-fingerprint
          "76EC F232 764E F614 4F2B  B89F 5B80 2705 FE37 8274")))))
```

Либо [передать в качестве параметра в Time Machine](https://guix.gnu.org/manual/en/html_node/Invoking-guix-time_002dmachine.html):

```console
$ guix time-machine --channels=test/channels.scm -- describe  # получить общую информацию об используемых каналах
$ guix time-machine --channels=test/channels.scm -- show socbot  # показать информацию о программном пакете `socbot`
```

# to/do:

- Guix требует подписи всех коммитов, но через пользовательский интерфейс GitLab это сделать пока [не возможно](https://gitlab.com/gitlab-org/gitlab/-/issues/19185)
-+ таким образом, функционал Merge Request пока использовать не получится

- возможно, стоит привести в соответствие имя распространяемого пакета (`socbot`) и репозитория (`chat-bot`)?
