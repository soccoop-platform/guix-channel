(define-module (soccoop packages)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (guix build-system pyproject)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))


(define-public socbot
  (package
    (name "socbot")
    (version "0.0.1")
    (source
     (origin
       (method git-fetch)
       (uri
         (git-reference
           (url "https://gitlab.com/soccoop-platform/chat-bot.git")
           (commit "a2539871f8b39664da77c94964d140a6230b4700")))
           ; (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
         (base32 "1rkgvprcnnshhn86b4jgpcwdz3ysvql7nnbwlq2plrjdhqwwcba7"))))
    (build-system pyproject-build-system)
    (arguments
      (list
        #:tests? #f))
    (inputs
      (list python-telethon))
    (native-inputs
      (list poetry))
    (home-page "https://gitlab.com/soccoop-platform/chat-bot")
    (synopsis "Automate communication in soccop community")
    (description "")
    (license license:wtfpl2)))
