(library (soccoop cells)
  (export
    cell-spa-demo)
  (import
    (scheme base)
    ; guix
    (guix build-system copy)
    (guix gexp)
    (guix git-download)
    (guix packages)
    (prefix (guix licenses) license:)))


(define cell-spa-demo
  (package
    (name "cell-spa")
    (version "0.0.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://gitlab.com/soccoop-platform/cell-spa.git")
              (commit (string-append "v" version))))
        (sha256
          (base32
            "1y7yfiw6i6nzp4v7xxrj5sabvmnm7q85fiigbrbnaz367g9kjihs"))
        (file-name (git-file-name name version))))
    (build-system copy-build-system)
    (arguments
      (list
        #:install-plan
        #~'(("dist/" "/srv/www/"))))
    (synopsis "Базовый сайт ячейки (\"табличный\" процесс)")
    (description #f)
    (license license:gpl3)
    (home-page "https://demo.soccop.dev/")))
