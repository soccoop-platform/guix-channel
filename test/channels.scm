(list
  (channel
    (name 'guix)
    (url "https://git.savannah.gnu.org/git/guix.git")
    (commit
      "220759226e93d76d8d80058f69f9d8b29714bbde"))
  (channel
    (name 'soccoop)
    (url "https://gitlab.com/soccoop-platform/guix-channel.git")
    (branch "main")
    (commit
      "733a9726b097171f971a1687cd51ab7fc465ef7a")
    (introduction
      (make-channel-introduction
        "733a9726b097171f971a1687cd51ab7fc465ef7a"
        (openpgp-fingerprint
          "76EC F232 764E F614 4F2B  B89F 5B80 2705 FE37 8274")))))
